project(
    'DesQ Clipboard',
    'c',
    'cpp',
    version: '0.0.9',
    license: 'GPLv3',
    meson_version: '>=0.59.0',
    default_options: [
        'cpp_std=c++17',
        'c_std=c11',
        'warning_level=2',
        'werror=false',
    ],
)

add_global_arguments( '-DPROJECT_VERSION="v@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

# If use_qt_version is 'auto', we will first search for Qt6.
# If all Qt6 packages are available, we'll use it. Otherwise,
# we'll switch to Qt5
if get_option('use_qt_version') == 'auto'
    # Check if Qt6 is available
    QtDeps = dependency(
    	'qt6',
    	modules: [ 'Core', 'Gui', 'Widgets' ],
    	required: false
    )

    DesQCore    = dependency( 'desq-core-qt6', required: false )
    DesQGui     = dependency( 'desq-gui-qt6', required: false )
    DesQWidgets = dependency( 'desq-widgets-qt6', required: false )

    DFApp       = dependency( 'df6application', required: false )
    DFClipboard = dependency( 'df6clipboard', required: false )
    DFXdg       = dependency( 'df6xdg', required: false )
    DFUtils     = dependency( 'df6utils', required: false )
    DFSettings  = dependency( 'df6settings', required: false )

    WayQt       = dependency( 'wayqt-qt6', required: false )

    Qt6_is_Found = QtDeps.found() and DFApp.found() and DesQCore.found() and DFSettings.found() and DesQCore.found() and DesQGui.found()
    Qt6_is_Found = Qt6_is_Found and DesQWidgets.found() and DFApp.found() and DFClipboard.found() and DFXdg.found() and DFUtils.found()
    Qt6_is_Found = Qt6_is_Found and DFSettings.found() and WayQt.found()

    if Qt6_is_Found
    	Qt = import( 'qt6' )
    	message( 'Using Qt6' )

    else
    	Qt = import( 'qt5' )
    	QtDeps = dependency(
    		'qt5',
    		modules: [ 'Core', 'Gui', 'Widgets' ],
    	)

        DesQCore    = dependency( 'desq-core' )
        DesQGui     = dependency( 'desq-gui' )
        DesQWidgets = dependency( 'desq-widgets' )

        DFApp       = dependency( 'df5application' )
        DFClipboard = dependency( 'df5clipboard' )
        DFXdg       = dependency( 'df5xdg' )
        DFUtils     = dependency( 'df5utils' )
        DFSettings  = dependency( 'df5settings' )

        WayQt       = dependency( 'wayqt' )
    endif

# User specifically wants to user Qt5
elif get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )

    Qt = import( 'qt5' )
    QtDeps = dependency(
        'qt5',
        modules: [ 'Core', 'Gui', 'Widgets' ],
    )

    DesQCore    = dependency( 'desq-core' )
    DesQGui     = dependency( 'desq-gui' )
    DesQWidgets = dependency( 'desq-widgets' )

    DFApp       = dependency( 'df5application' )
    DFClipboard = dependency( 'df5clipboard' )
    DFXdg       = dependency( 'df5xdg' )
    DFUtils     = dependency( 'df5utils' )
    DFSettings  = dependency( 'df5settings' )

    WayQt       = dependency( 'wayqt' )

# User specifically wants to user Qt6
elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )

    QtDeps = dependency(
    	'qt6',
    	modules: [ 'Core', 'Gui', 'Widgets' ],
    	required: false
    )

    DesQCore    = dependency( 'desq-core-qt6' )
    DesQGui     = dependency( 'desq-gui-qt6' )
    DesQWidgets = dependency( 'desq-widgets-qt6' )

    DFApp       = dependency( 'df6application' )
    DFClipboard = dependency( 'df6clipboard' )
    DFXdg       = dependency( 'df6xdg' )
    DFUtils     = dependency( 'df6utils' )
    DFSettings  = dependency( 'df6settings' )

    WayQt       = dependency( 'wayqt-qt6' )
endif

Deps = [ QtDeps, DesQCore, DesQGui, DesQWidgets, WayQt, DFApp, DFSettings, DFClipboard, DFXdg, DFUtils ]

Includes = [
    'UI'
]

Headers = [
    'Global.hpp',
    'UI/Clipboard.hpp',
    'UI/ClipboardSNI.hpp',
    'UI/ClipboardView.hpp'
]

Sources = [
    'Main.cpp',
    'UI/Clipboard.cpp',
    'UI/ClipboardSNI.cpp',
    'UI/ClipboardView.cpp'
]

Mocs = Qt.compile_moc(
    headers : Headers,
    dependencies: [ QtDeps ]
)

Resources = Qt.compile_resources(
    name: 'desqclip_rcc',
    sources : 'icons/icons.qrc'
)

desqclip = executable(
    'desq-clipboard', [ Sources, Mocs, Resources ],
    dependencies: Deps,
    include_directories: [ Includes ],
    install: true,
    install_dir: join_paths( get_option( 'libdir' ), get_option( 'libexecdir' ), 'desq' )
)

install_data(
    'Clipboard.conf',
    install_dir: join_paths( get_option( 'datadir' ), 'desq', 'configs' ),
)

install_data(
    'icons/desq-clipboard.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)

install_data(
    'icons/desq-clipboard.png',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', '256x256', 'apps' ),
)

summary = [
	'',
	'---------------------',
	'DesQ Clipboard  @0@'.format( meson.project_version() ),
	'---------------------',
	''
]
message( '\n'.join( summary ) )
