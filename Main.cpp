/**
 * This file is a part of DesQ Clipboard.
 * DesQ Clipboard is the Wlroots-based Clipboard Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "Clipboard.hpp"

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <DFApplication.hpp>

/** By default we will not disable selections */
bool DesQ::Clipboard::disableSelections = false;

DFL::Settings *clipSett;
int main( int argc, char **argv ) {
    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Clipboard" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setQuitOnLastWindowClosed( false );

    clipSett = DesQ::Utils::initializeDesQSettings( "Clipboard", "Clipboard" );

    /**
     * If we're restoring the session, retrieve the values of the session variables
     */
    if ( clipSett->value( "RestoreSession" ) ) {
        /** Disable the Selections */
        QVariant value = clipSett->value( "Session/DisableSelections" );

        if ( value.isValid() ) {
            DesQ::Clipboard::disableSelections = value.toBool();
        }
    }

    DesQ::Clipboard::UI *cb = new DesQ::Clipboard::UI();

    cb->startTray();
    qDebug() << "Monitoring";

    return app.exec();
}
