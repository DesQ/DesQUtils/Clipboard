/**
 * This file is a part of DesQ Clipboard.
 * DesQ Clipboard is the Wlroots-based Clipboard Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <DFClipboardManager.hpp>

namespace DesQ {
    namespace Clipboard {
        class UI;
        class View;
    }
}

class ClipboardItem;

class DesQ::Clipboard::UI : public QMainWindow {
    Q_OBJECT;

    public:
        UI();
        ~UI();

        void startTray();

    private:
        /** Clipboard Manager Backend */
        DFL::Clipboard::Manager *clipMgr;

        /** View classes to show the Clipboard and Selection items */
        DesQ::Clipboard::View *clipboardView;
        DesQ::Clipboard::View *selectionView;

        /** Create a GUI and set it's properties */
        void createUI();
        void setWindowProperties();

        void reloadSettings( QString, QVariant );

    protected:
        void keyPressEvent( QKeyEvent *kEvent );
};
