/**
 * This file is a part of DesQ Clipboard.
 * DesQ Clipboard is the Wlroots-based Clipboard Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Clipboard.hpp"
#include "ClipboardSNI.hpp"
#include "ClipboardView.hpp"

#include <DFXdg.hpp>
#include <DFApplication.hpp>

#include <wayqt/Registry.hpp>
#include <wayqt/DataControl.hpp>


DesQ::Clipboard::UI::UI() : QMainWindow() {
    QString xdgPath = DFL::XDG::xdgDataHome() + "desq/Clipboard/";

    clipMgr = new DFL::Clipboard::Manager( xdgPath );

    clipMgr->setSelectionDisabled( false );
    clipMgr->setSelectionIgnored( (bool)clipSett->value( "IgnoreSelections" ) );

    clipMgr->setStoreHistory( (bool)clipSett->value( "StoreHistory" ) );
    clipMgr->setHistorySize( (int)clipSett->value( "HistorySize" ) );

    clipMgr->setLargeDataSizeLimit( (int)clipSett->value( "MaxDataSize" ) );

    createUI();
    setWindowProperties();

    connect( clipSett, &DFL::Settings::settingChanged, this, &DesQ::Clipboard::UI::reloadSettings );
}


DesQ::Clipboard::UI::~UI() {
    delete clipMgr;
}


void DesQ::Clipboard::UI::createUI() {
    /* Clipboard/Selection buttons */
    QPushButton *btnC = new QPushButton( QIcon( ":/icons/keyboard.png" ), "Clipboard" );

    btnC->setCheckable( true );
    btnC->setChecked( false );

    QPushButton *btnS = new QPushButton( QIcon( ":/icons/mouse.png" ), "Selection" );

    btnS->setCheckable( true );
    btnS->setChecked( false );

    QButtonGroup *group = new QButtonGroup();

    group->setExclusive( true );
    group->addButton( btnC, 0 );
    group->addButton( btnS, 1 );

    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addStretch();
    btnLyt->addWidget( btnC );
    btnLyt->addWidget( btnS );
    btnLyt->addStretch();

    /* Clipboard and Selection Views */
    clipboardView = new DesQ::Clipboard::View( DFL::Clipboard::Clipboard );
    selectionView = new DesQ::Clipboard::View( DFL::Clipboard::Selection );

    /** Disable the selection items if we have to ignore primary */
    if ( clipSett->value( "IgnoreSelections" ) ) {
        selectionView->setDisabled( true );
    }
    /** Otherwise, disable the selection items if we have to disable primary */
    else if ( DesQ::Clipboard::disableSelections ) {
        selectionView->setDisabled( true );
    }

    QStackedWidget *views = new QStackedWidget();

    views->addWidget( clipboardView );
    views->addWidget( selectionView );

    /* Base and it's Layout */
    QWidget     *base    = new QWidget();
    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->addLayout( btnLyt );
    baseLyt->addWidget( views );
    base->setLayout( baseLyt );
    setCentralWidget( base );

    connect( group,         &QButtonGroup::idClicked,      views,   &QStackedWidget::setCurrentIndex );
    connect( clipboardView, &DesQ::Clipboard::View::offer, clipMgr, &DFL::Clipboard::Manager::requestOffer );
    connect( selectionView, &DesQ::Clipboard::View::offer, clipMgr, &DFL::Clipboard::Manager::requestOffer );

    connect(
        clipMgr, &DFL::Clipboard::Manager::updateView, [ = ] ( int id ) {
            if ( id == 0 ) {
                clipboardView->reloadView();
            }

            else {
                selectionView->reloadView();
            }
        }
    );

    btnC->click();
}


void DesQ::Clipboard::UI::setWindowProperties() {
    /* Title */
    setWindowTitle( "DesQ Clipboard" );

    /* Set the windowFlags */
    // setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint |
    // Qt::BypassWindowManagerHint );
    setWindowFlags( Qt::WindowStaysOnTopHint );

    /* Minimum window size */
    setMinimumSize( QSize( 360, 450 ) );
}


void DesQ::Clipboard::UI::startTray() {
    DesQ::Clipboard::SNI *icon = new DesQ::Clipboard::SNI( this );

    icon->show();
}


void DesQ::Clipboard::UI::reloadSettings( QString key, QVariant value ) {
    if ( key == "IgnoreSelections" ) {
        /** Disable the Selection UI */
        if ( value.toBool() ) {
            selectionView->setDisabled( true );
        }

        /** Enable the Selection UI */
        else {
            selectionView->setEnabled( true );
        }
    }

    else if ( key == "Session/DisableSelections" ) {
        /** We've already restored the session. These are live changes. */

        /** Save the change */
        DesQ::Clipboard::disableSelections = value.toBool();

        /** Apply the change to the UI */
        if ( value.toBool() ) {
            selectionView->setDisabled( true );
            clipMgr->requestOffer( DFL::Clipboard::Selection, -1 );
        }

        /** Enable the Selection UI */
        else {
            selectionView->setEnabled( true );
            clipMgr->requestOffer( DFL::Clipboard::Selection, 0 );
        }
    }

    else if ( key == "RestoreSession" ) {
        /** If it's true, we have to restore the session variables and the UI */
        if ( value.toBool() ) {
            QVariant value;

            /** Disable the Selections */
            value = clipSett->value( "Session/DisableSelections" );

            if ( value.isValid() ) {
                DesQ::Clipboard::disableSelections = value.toBool();

                /** Apply the change to the UI */
                if ( value.toBool() ) {
                    selectionView->setDisabled( true );
                    // #warning "Trigger the Clipboard Manager to disable the current
                    // selection"
                }

                /** Enable the Selection UI */
                else {
                    selectionView->setEnabled( true );
                    // #warning "Trigger the Clipboard Manager to enable the previous
                    // selection"
                }
            }
        }
    }
}


void DesQ::Clipboard::UI::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        hide();
    }

    QWidget::keyPressEvent( kEvent );
}
